﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineRates.Core
{
    interface IBankRatesProvider
    {
        Bank GetBank();
        Task<Bank> GetBanks();
    }
}
