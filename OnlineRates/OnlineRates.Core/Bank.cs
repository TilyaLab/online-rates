﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineRates.Core
{
    public class Bank
    {
        public Bank([NotNull]string name, [NotNull] Currency currency)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Currency = currency ?? throw new ArgumentNullException(nameof(currency));
        }
        public int Id { get; protected set; }
        public string Name { get; protected set; }
        public Currency Сurrencies { get; protected set; }
    }
}
