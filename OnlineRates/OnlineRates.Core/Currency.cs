﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineRates.Core
{
    public class Currency
    {
        public Currency([NotNull] string name, [NotNull] string shortName)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            ShortName = shortName ?? throw new ArgumentNullException(nameof(shortName));
        }

        public int Id { get; protected set; }
        public string Name { get; protected set; }
        public string ShortName { get; protected set; }
        public double Rates { get; protected set; }
    }
}
